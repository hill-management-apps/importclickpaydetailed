﻿using System;
using System.Diagnostics.Tracing;
using System.IO;

namespace ImportClickPayDetailed
{
    public class Logger
    {
        string fileName = string.Empty;
        bool echo = false;

        public Logger(string _fileName, bool _echo)
        {
            fileName = _fileName;
            echo = _echo;
        }

        public void LogMessage(string msg)
        {
            try
            {
                File.AppendAllText(fileName, string.Format("{0}\t{1}\n", DateTime.Now.ToLongTimeString(), msg));

                if (echo)
                    Console.WriteLine(string.Format("{0}\t{1}", DateTime.Now.ToLongTimeString(), msg));
            }
            catch
            {
                throw;
            }
        }
    }
}
