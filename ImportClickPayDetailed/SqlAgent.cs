﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;



namespace ImportClickPayDetailed
{
    public class SqlAgent
    {
        private string userId = string.Empty;
        const string CONNECTION_STRING = @"data source=HSSQL2012\HSSQLSERVER;initial catalog=HOMESALES;Integrated Security=true;";
        Logger logger = null;

        public SqlAgent(Logger _logger)
        {
            logger = _logger;
        }

        public DataSet ExecuteQuery(string sql)
        {
            DataSet ds = new DataSet();
            return this.ExecuteQuery(sql, CONNECTION_STRING);
        }

        public DataSet ExecuteQuery(string sql, string connectString)
        {
            DataSet ds = new DataSet();
            //logger.LogMessage("Here in SqlAgent.ExecuteQuery");

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, connectString);
                da.Fill(ds);
                return ds;
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
        }

        public void ExecuteNonQuery(string sql)
        {
            SqlConnection cn = null;
            SqlTransaction trans = null;

            //logger.LogMessage("Here in SqlAgent.ExecuteNonQuery");
            
            try
            {
                cn = new SqlConnection(CONNECTION_STRING);
                SqlCommand command = new SqlCommand(sql, cn);
                command.Connection.Open();
                trans = cn.BeginTransaction();
                command.Transaction = trans;
                command.ExecuteNonQuery();
                trans.Commit();
            }
            catch (Exception _ex)
            {
                trans.Rollback();
                throw _ex;
            }
        }
    }
}
