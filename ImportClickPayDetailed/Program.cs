﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelDataReader;
using System.Data;
using System.IO;

namespace ImportClickPayDetailed
{
    class Program
    {
        //Detailed_Transaction_Report_12232020_0602.xls
        static void Main(string[] args)
        {
            DataSet ds = null;
            DataTable dt = null;

            Logger logger = new Logger(string.Format("{0}\\log.log", Environment.CurrentDirectory), true);
            SqlAgent sa = new SqlAgent(logger);
            string sql = string.Empty;

            int RowsAdded = 0;
            int RowsRejected = 0;

            string filename = string.Format("{0}\\Detailed_Transaction_Report_12232020_0602.xls", Environment.CurrentDirectory);
            logger.LogMessage(string.Format("About to open Excel File: {0}", filename));

            try
            {
                using (var stream = File.Open(filename, FileMode.Open, FileAccess.Read))
                {
                    IExcelDataReader reader;
                    reader = ExcelDataReader.ExcelReaderFactory.CreateReader(stream);

                    var conf = new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = _ => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = false
                        }
                    };

                    ds = reader.AsDataSet(conf);
                }

                dt = ds.Tables[0];

                // This is the beginning of the SQL statement ONLY.  Each time a batch is executed,
                // It will revert back to this.
                string sqlBegin = "insert into hs_clickpay_detailed ( " +
                    "[LLC_Association] " +
                    ",[Date] " +
                    ",[Property] " +
                    ",[Building_ID] " +
                    ",[Resident_Name] " +
                    ",[Payer] " +
                    ",[Email_Address] " +
                    ",[Ledger_Name] " +
                    ",[Site] " +
                    ",[Confirmation_Number] " +
                    ",[Amount] " +
                    ",[Merchant_Fees] " +
                    ",[Reason_Code] " +
                    ",[Comments] " +
                    ",[Transaction_Detail_Comments] " +
                    ",[Settlement_Date] " +
                    ",[Description] " +
                    ",[Phone] " +
                    ",[Payment_Method] " +
                    ",[Account_Number] " +
                    ",[Account_Token] " +
                    ",[Product_Description] " +
                    ",[Product_ID] " +
                    ",[Merchant] " +
                    ",[Merchant_Tag] " +
                    ",[CheckNumber] " +
                    ",[GL_Code] " +
                    ",[Sub_Account] " +
                    ",[Lockbox_Check_Type] " +
                    ",[Account_Created_By] " +
                    ",[Last_4_Digits__Payment_Method_] " +
                    ",[Batch_ID__ClickPay_] " +
                    ",[Software_BatchID]) VALUES ";

                sql = sqlBegin;
                bool firstTimeThru = true;
                int counter = 1;

                // Loop thru resulting dataset.
                foreach (DataRow dr in dt.Rows)
                {
                    // skip the first row of headers.
                    if (firstTimeThru)
                    {
                        firstTimeThru = false;
                        continue;
                    }

                    // Making sure the single quote in names doesn't play hob with the SQL statement.
                    if (dr[4].ToString().Contains("'"))
                    {
                        dr[4] = dr[4].ToString().Replace("'", "`");
                    }
                    if (dr[5].ToString().Contains("'"))
                    {
                        dr[5] = dr[5].ToString().Replace("'", "`");
                    }
                    if (dr[7].ToString().Contains("'"))
                    {
                        dr[7] = dr[7].ToString().Replace("'", "`");
                    }

                    // Convert the date strings into actual dates in the output table.
                    int iTmp = dr[1].ToString().IndexOf(" ", 14);
                    string sTmp = dr[1].ToString().Substring(0, iTmp);
                    DateTime dtDate = DateTime.Parse(sTmp);
                    string sDate = dtDate.ToString("MM/dd/yyyy hh:mm:ss");

                    sTmp = dr[15].ToString().Substring(0, iTmp);
                    DateTime dtSett = DateTime.Parse(sTmp);
                    string sSett = dtSett.ToString("MM/dd/yyyy hh:mm:ss");

                    // Formatting the Amount into a number.  It might be negative, hense the check for a "-".
                    decimal amt = 0;
                    if (dr[10].ToString().IndexOf("-") >= 0)
                        amt = decimal.Parse(string.Format("-{0}", dr[10].ToString().Substring(2)));
                    else
                        amt = decimal.Parse(dr[10].ToString().Substring(1));

                    // If the batchID is null or empty, look it up
                    string batchID = dr[32].ToString();
                    if (batchID == "")
                    {
                        batchID = FetchMissingBatchID(logger, dr[9].ToString());
                    }

                    // Determine if a row with matching confirmation number already exists
                    bool okToInsert = DetermineConfirmationExists(logger, dr[9].ToString());

                    if (okToInsert)
                    {
                        sql += string.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', {10}, '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', '{29}', '{30}', '{31}', '{32}'), ",
                            dr[0].ToString().Length > 50 ? dr[0].ToString().Substring(0, 50) : dr[0].ToString(),
                            sDate,
                            dr[2].ToString().Length > 50 ? dr[2].ToString().Substring(0, 50) : dr[2].ToString(),
                            dr[3].ToString(),
                            dr[4].ToString().Length > 50 ? dr[4].ToString().Substring(0, 50) : dr[4].ToString(),
                            dr[5].ToString().Length > 50 ? dr[5].ToString().Substring(0, 50) : dr[5].ToString(),
                            dr[6].ToString().Length > 50 ? dr[6].ToString().Substring(0, 50) : dr[6].ToString(),
                            dr[7].ToString() == string.Empty ? " " : dr[7].ToString(),
                            dr[8].ToString().Length > 50 ? dr[8].ToString().Substring(0, 50) : dr[8].ToString(),
                            dr[9].ToString().Length > 50 ? dr[9].ToString().Substring(0, 50) : dr[9].ToString(),
                            amt.ToString(),
                            dr[11].ToString().Length > 50 ? dr[11].ToString().Substring(0, 50) : dr[11].ToString(),
                            dr[12].ToString().Length > 1 ? dr[12].ToString().Substring(0, 1) : dr[12].ToString(),
                            dr[13].ToString().Length > 100 ? dr[13].ToString().Substring(0, 100) : dr[13].ToString(),
                            dr[14].ToString().Length > 100 ? dr[14].ToString().Substring(0, 100) : dr[14].ToString(),
                            sSett,
                            dr[16].ToString().Length > 50 ? dr[16].ToString().Substring(0, 50) : dr[16].ToString(),
                            dr[17].ToString().Length > 50 ? dr[17].ToString().Substring(0, 50) : dr[17].ToString(),
                            dr[18].ToString().Length > 50 ? dr[18].ToString().Substring(0, 50) : dr[18].ToString(),
                            dr[19].ToString().Length > 50 ? dr[19].ToString().Substring(0, 50) : dr[19].ToString(),
                            dr[20].ToString().Length > 50 ? dr[20].ToString().Substring(0, 50) : dr[20].ToString(),
                            dr[21].ToString().Length > 1 ? dr[21].ToString().Substring(0, 1) : dr[21].ToString(),
                            dr[22].ToString().Length > 50 ? dr[22].ToString().Substring(0, 50) : dr[22].ToString(),
                            dr[23].ToString().Length > 50 ? dr[23].ToString().Substring(0, 50) : dr[23].ToString(),
                            dr[24].ToString().Length > 50 ? dr[24].ToString().Substring(0, 50) : dr[24].ToString(),
                            dr[25].ToString().Length > 1 ? dr[25].ToString().Substring(0, 1) : dr[25].ToString(),
                            dr[26].ToString().Length > 1 ? dr[26].ToString().Substring(0, 1) : dr[26].ToString(),
                            dr[27].ToString().Length > 1 ? dr[27].ToString().Substring(0, 1) : dr[27].ToString(),
                            dr[28].ToString().Length > 1 ? dr[28].ToString().Substring(0, 1) : dr[28].ToString(),
                            dr[29].ToString().Length > 50 ? dr[29].ToString().Substring(0, 50) : dr[29].ToString(),
                            dr[30].ToString(),
                            dr[31].ToString().Length > 50 ? dr[31].ToString().Substring(0, 50) : dr[31].ToString(),
                            batchID);

                        // Batch up the INSERT statements. 1000 is the max, so I'm stopping at 900.
                        if (counter % 900 == 0)
                        {
                            logger.LogMessage(string.Format("Counter: {0} is a multiple of 900.  Execute this batch of SQL.", counter));

                            sql = sql.Substring(0, sql.Length - 2);     // Trying to strip the trailing comma.  Subtract 4 because of spaces after the comma.
                            sql += "; ";                                // Then add a trailing semi to indicate the end of the batch.

                            sa.ExecuteNonQuery(sql);

                            sql = sqlBegin;
                        }

                        counter++;
                        RowsAdded++;
                    }
                    else
                    {
                        // If it's NOT okToInsert, that means the confirmation nunmber DOES exist.  Skip the row.
                        RowsRejected++;
                        logger.LogMessage(string.Format("Row with Confirmation Number: {0} already exists.  Skipping.", dr[9].ToString()));
                    }
                }

                // Follow up with the finall batch
                sql = sql.Substring(0, sql.Length - 2);     // Trying to strip the trailing comma.  Subtract 4 because of spaces after the comma.
                sql += "; ";                                // Then add a trailing semi to indicate the end of the batch.

                sa.ExecuteNonQuery(sql);

                // Log the final bit of information.
                logger.LogMessage(string.Format("Number of rows added: {0}", RowsAdded));
                logger.LogMessage(string.Format("Number of rows rejected: {0}", RowsRejected));
            }
            catch (Exception ex)
            {
                logger.LogMessage(ex.Message);
            }

            Console.WriteLine("Press any key to exit...");
            Console.Read();
        }

        private static bool DetermineConfirmationExists(Logger logger, string confNum)
        {
            //logger.LogMessage("Here in DetermineConfirmationExists");
            bool retVal = true;

            try
            {
                SqlAgent sa = new SqlAgent(logger);
                DataSet ds = sa.ExecuteQuery(string.Format("select count(*) from hs_clickpay_detailed where confirmation_number = '{0}'", confNum));
                if (ds.Tables[0].Rows[0][0].ToString() == "1")
                    retVal = false;
            }
            catch
            {
                throw;
            }
            return retVal;
        }

        private static string FetchMissingBatchID(Logger logger, string confNum)
        {
            string retVal = string.Empty;   // carve out the actual conf num less the '/1'.
            string cNum = confNum.Substring(0, 18);
            string sql = string.Format("select rmbatchid from rmledg where paymentranid like '{0}%'", cNum);

            try
            {
                SqlAgent sa = new SqlAgent(logger);
                DataSet ds = sa.ExecuteQuery(sql);
                if (ds.Tables[0].Rows.Count > 0)
                    retVal = ds.Tables[0].Rows[0][0].ToString();
            }
            catch
            {
                throw;
            }
            
            return retVal;
        }
    }
}
